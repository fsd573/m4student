package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	@Autowired
	StudentDao studentDao;
	
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		return studentDao.getStudents();
	}
	
	@GetMapping("getStudentById/{studentId}")
	public Student getStudentById(@PathVariable int studentId) {
		return studentDao.getStudentById(studentId);
	}
	
	@GetMapping("getStudentByName/{studentName}")
	public Student getStudentByName(@PathVariable String studentName) {
		return studentDao.getStudentByName(studentName);
	}
	
	@GetMapping("stuLogin/{emailId}/{password}")
	public Student studentLogin(@PathVariable String emailId, @PathVariable String password) {
		return studentDao.studentLogin(emailId, password);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student) {
		return studentDao.addStudent(student);
	}
	
	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student student) {
		return studentDao.updateStudent(student);
	}
	
	@DeleteMapping("deleteStudentById/{studentId}")
	public String deleteStudentById(@PathVariable int studentId) {
		studentDao.deleteStudentById(studentId);
		return "Student with stuId:" + studentId + " Deleted Successfully!!!";
	}
}



