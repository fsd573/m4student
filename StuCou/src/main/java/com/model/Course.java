package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {

	@Id
	private int couId;			
	private String couName;	
	private int duration;
	
	//Implementing Mapping Between Course and Student
		@JsonIgnore	//UnComment this later on
		@OneToMany(mappedBy="course")
		List<Student> stuList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int couId, String couName, int duration) {
		this.couId = couId;
		this.couName = couName;
		this.duration = duration;
	}
	
	//Generating Getter for stuList Variable
		public List<Student> getStuList() {
			return stuList;
		}

	//Generating Setter for stuList Variable
		public void setStuList(List<Student> stuList) {
			this.stuList = stuList;
		}

	public int getCouId() {
		return couId;
	}

	public void setCouId(int couId) {
		this.couId = couId;
	}

	public String getCouName() {
		return couName;
	}

	public void setCouName(String couName) {
		this.couName = couName;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}	
	
}
