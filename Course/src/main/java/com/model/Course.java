package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Course {
	//Entity : Making Course Class as a Course Table under Database
		//Id     : Making courseId Column as a Primary Key Column
		//Column : Providing our own column name instead of variable name as the column name
		//GeneratedValue: Auto_Increment
		
		@Id@GeneratedValue
		private int courseId;			
		
		@Column(name="courseName")
		private String courseName;	
		private double price;
	
	public Course() {
		super();
	}

	public Course(int courseId, String courseName, double price) {
		this.courseId = courseId;
		this.courseName = courseName;
		this.price = price;
	}

	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", courseName=" + courseName + ", price=" + price + "]";
	}	
}
