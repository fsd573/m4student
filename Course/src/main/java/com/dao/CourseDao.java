package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service
public class CourseDao {
	//Implementing Dependency Injection for CourseRepository
		@Autowired
		CourseRepository courseRepository;
		
		public List<Course> getAllCourses() {
			return courseRepository.findAll();
		}

		public Course getCourseById(int courseId) {
			Course course = new Course(0, "Course Not Found!!!", 0.0);
			return courseRepository.findById(courseId).orElse(course);
		}

		public List<Course> getCourseByName(String courseName) {
			return courseRepository.findByName(courseName);
		}

		public Course addCourse(Course course) {
			return courseRepository.save(course);
		}
		
//		public Course updateCourse(Course course) {
//		return courseRepository.save(course);
//	}

		public Course updateCourseById(int courseId, Course updatedCourse) {
			Course existingCourse = courseRepository.findById(courseId).orElse(null);
			if (existingCourse != null) {
				existingCourse.setCourseName(updatedCourse.getCourseName());
	            existingCourse.setPrice(updatedCourse.getPrice());
	            return courseRepository.save(existingCourse);
	        } else {
	        	return new Course(0, "Course Not Found!!!", 0.0);
			}
		}

		public void deleteCourseById(int courseId) {
			courseRepository.deleteById(courseId);
			
		}

}
